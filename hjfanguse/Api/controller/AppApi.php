<?php
namespace app\Api\controller; 
use think\Controller;
use think\Url;
use think\Config;
use think\Page;
use think\Verify;
use think\Image;
use think\Db;
use app\home\logic\UsersLogic;
use app\home\model\Message;
use app\admin\logic\GoodsPromFactory;
use think\AjaxPage;
use app\home\logic\CartLogic;
use app\home\logic\GoodsLogic;
use app\home\logic\OrderLogic;
use app\admin\model\GroupBuy;

header('content-type:application:json;charset=utf8');  
header('Access-Control-Allow-Origin:*');  
header('Access-Control-Allow-Methods:POST');  
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE');

class appApi{
	
	public function __construct(){
		 $this->cartLogic = new \app\home\logic\CartLogic();
	}
	//获取下拉分页商品数据029d8f2e164970f1f1e75e4270136370
	public function index()
		{
			//echo sha1(md5('lyp!123'));exit;
			$page=isset($_REQUEST['page'])?intval($_REQUEST['page']):1;
			$cat_id=isset($_POST['cat_id'])?intval($_POST['cat_id']):0;
			$cat_id_str='';
						if(!empty($cat_id)){
							$cat_id_str='  and cat_id='.$cat_id;
						}
			$count=M('goods')->where('is_real=1 and is_on_sale=1'.$cat_id_str.' and store_count > 0')->count();
			$data['data']=array();
			
			//$page=$page>=1?$page:1;
			$page=$page > 0 ?$page : 1;
			$number=20;
			$start=($page-1)*$number;
			$end=$start+$number;
			
				if($start < $count)
					{
						if($end > $count)
							{
								$number=$number-($end-$count);
							}
						
						$where=' is_real=1 and is_on_sale=1'.$cat_id_str.' order by goods_id desc';
						$limit=' limit '.$start.','.$number;
						$sql='select sales_sum,is_free_shipping,is_on_sale,is_real,original_img,goods_content,shop_price,weight,comment_count,store_count,goods_name,goods_id from '.C('database.prefix').'goods where  '.$where.$limit;
						$result=Db::query($sql);
						if(!empty($result))
							{
								
								/* foreach($result as $k => $v)
									{
										$result[$k]['original_img']='http://127.0.0.1/tpshop'.$result[$k]['original_img'];
									} */
								$data['data']=$result;
							}
					}
			$data['status']=1;
			$data['total']=$count;
			$data=$this->ajaxReturn($data);
			echo $data;
		}
		
	//返回json数据
	public function ajaxReturn($data=array())
		{
			return json_encode($data);
		}
	
	//登陆
	public function login(){
		$user=isset($_POST['username'])?trim($_POST['username']):'';
		$pwd=isset($_POST['password'])?trim($_POST['password']):'';
		$code=isset($_POST['nickname'])?trim($_POST['nickname']):'yb34';
		$mrand=isset($_POST['mrand'])?$_POST['mrand']:0;
		session_id(sha1($mrand));//不同用户该值要设置不同，不然会多个用户使用同一个sessionID，导致有的用户无法正常使用。
		$result=array('flag'=>0,'msg'=>'','data'=>array(),'code'=>0);
		//session_id(sha1($mrand));
		if(!session('?'.sha1('islogin')))
			{
				$verify = new Verify();
				if($verify->check($code,"html5client"))
					{
						if(empty($user))
							{
								$result['msg']='用户名不能为空！';
							}
							else if(empty($pwd)){
								$result['msg']='密码不能为空！';
							}
							else
								{
									$exists=M('users')->where('(email="'.$user.'" or mobile="'.$user.'") and password="'.sha1(md5($pwd)).'"')->count();
									if($exists)
										{
											$cookies=sha1(md5($user.time().mt_rand().$pwd).'SessionId');
											$_SESSION[sha1('islogin')]=sha1($user);
											$result['msg']='登陆成功！';
											$result['flag']=1;
										}
										else
											{
												$result['msg']='用户名或密码错误！';
											}
								}
						
					}
					else
						{
							$result['msg']='验证码错误！'.$code;
						}
			}
			else
				{
					$result['msg']='你已经登陆！';
					$result['flag']=1;
				}
		echo json_encode($result);
	}	
	
	//注册
	public function regcheck(){
		$user=isset($_POST['username'])?trim($_POST['username']):'sdfs';
		$pwd=isset($_POST['password'])?trim($_POST['password']):'sdafdsd';
		$code=isset($_POST['nickname'])?trim($_POST['nickname']):'adfas';
		$checkpwd=isset($_POST['checkpwd'])?trim($_POST['checkpwd']):'asadfas';
		$mrand=isset($_POST['mrand'])?$_POST['mrand']:0;
		
		session_id(sha1($mrand));//不同用户该值要设置不同，不然会多个用户使用同一个sessionID，导致有的用户无法正常使用。
		$result=array('flag'=>0,'msg'=>'','data'=>array(),'code'=>0);
		if(!session('?'.sha1('islogin')))
			{
				$verify = new Verify();
				if($verify->check($code,"html5client"))
					{
					if(!$this->isEmail($user) && !$this->isMobile($user))
							{
								$result['msg']='用户名格式不对！';
							}
							elseif(!$this->isPassword($pwd))
							{
								$result['msg']='密码格式不对！';
							}
							elseif($checkpwd != $pwd)
							{
								$result['msg']='两次输入的密码不一样！';
							}
							else
								{
									$exists=M('users')->where('email="'.$user.'" or mobile="'.$user.'"')->count();
									
									if(!$exists)
										{
											$sql='';
											$data=array();
											if($this->isEmail($user))
											{
												$sql='insert into tp_users(email,password) values("'.$user.'","'.sha1(md5($pwd)).'")';
												$data['email']=$user;
												$data['password']=sha1(md5($pwd));
											}
											elseif($this->isMobile($user))
											{
												$sql='insert into tp_users(mobile,password) values("'.$user.'","'.sha1(md5($pwd)).'")';
												$data['mobile']=$user;
												$data['password']=sha1(md5($pwd));
											}
											else
											{
												
											}
											
											if(M('users')->add($data))//->add($data)
												{
													$result['msg']='注册成功！请到登陆页面登陆！';
													$result['flag']=1;
												}
												else{
													$result['msg']='注册失败！';
												}
												
										}
										else
											{
												$result['msg']='用户名已经存在！';
											}
								}
						
					}
					else
						{
							$result['msg']='验证码错误！'.$code;
						}
			}
			else
				{
					$result['msg']='你已经登陆！';
					$result['flag']=1;
				}
		echo json_encode($result);
	}	
	
	function isEmail($str){
		$preg='/([a-zA-Z0-9\.\-])+@(([a-zA-Z0-9])\.)+([a-zA-Z0-9]{2,4})+/';
		return preg_match($preg,$str);
	}
	
	function isMobile($str){
		$preg='/1[0-9]{10}/';
		return preg_match($preg,$str);
	}
	
	function isPassword($str){
		$preg='/^(?=.*[a-zA-Z])(?=.*\d)(?=.*[~!@#$%^&*()_+`\-={}:";\'<>?,.\/]).{6,18}$/';
		return preg_match($preg,$str);
	}
	
	function isCode($str){
		$preg='/[a-z0-9]{4}/i';
		return preg_match($preg,$str);
	}
	
	//验证码生成
	 public function vertify()
		{
			$config = array(
				'fontSize' => 30,
				'length' => 4,
				'useCurve' => true,
				'useNoise' => false,
				'reset' => false
			);    
			
			session_id(sha1($_REQUEST['r']));
			//session_start();
			$Verify = new Verify($config);
			$Verify->entry("html5client");
			exit();
		}	
		
	//我的收藏	
	public function collect_list()
		{
			header('Access-Control-Allow-Origin:*');  
			$this->user_id=isset($_POST['user'])?intval($_POST['user']):0;
			$userLogic = new UsersLogic();
			$data = $userLogic->get_goods_collect($this->user_id);
			$result=$data;
			if(!empty($result))
				{
					$result['flag']=1;
				}
				else{
					$result['flag']=0;
				}
			echo json_encode($result);
		}
		
	//活动消息	
	public function message_notice()
		{
			$type = I('type', 0);
			$user_logic = new UsersLogic();
			$message_model = new Message();
			$user_sys_message=array();
			if ($type == 1) {
				//系统消息
				$user_sys_message = $message_model->getUserMessageNotice();
				$user_logic->setSysMessageForRead();
			} else if ($type == 2) {
				//活动消息：后续开发
				$user_sys_message = array();
			} else {
				//全部消息：后续完善
				$user_sys_message = $message_model->getUserMessageNotice();
			}
			$data['data']=$user_sys_message;
			if(!empty($user_sys_message))
				{
					$data['flag']=1;
				}
				else
				{
					$data['flag']=0;
				}
			echo json_encode($data);

		}	
		
	public function getimgB(){
		$url=isset($_POST['img'])?$_POST['img']:'';
		$img=array();
		$img['flag']=0;
		if(!empty($url)){
			$img['flag']=1;
			$img['img']=base64_encode(file_get_contents($url));
		}
		else{
			$img['img']='';
		}
		
		echo json_encode($img);
		
		
		//alert();
		//echo '<img src="data:image/jpeg;base64,'.base64_encode(file_get_contents("http://www.tpshop.com/public/upload/goods/thumb/145/goods_thumb_145_400_400.jpeg")).'"/>';
	}
		
		
	 /**
     * 浏览记录
     */
    public function visitlog()
    {
		$this->user_id=isset($_POST['user'])?intval($_POST['user']):2592;
		$p=isset($_POST['p'])?intval($_POST['p']):2;
		
        $count = M('goods_visit')->where('user_id', $this->user_id)->count();
        $page = new Page($count, 20);
		if($p>1){
			$page->firstRow=($p-1)*20;
			if(($page->firstRow + $page->listRows) > $count && ($count-$page->firstRow)>0){
				$page->listRows=($count-$page->firstRow);
			}
		}
        $visit = M('goods_visit')->alias('v')
            ->field('v.visit_id, v.goods_id, v.visittime, g.goods_name, g.shop_price, g.cat_id,g.original_img')
            ->join('__GOODS__ g', 'v.goods_id=g.goods_id')
            ->where('v.user_id', $this->user_id)
            ->order('v.visittime desc')
            ->limit($page->firstRow, $page->listRows)
            ->select();
		//echo M('goods_visit')->getLastSql();
        /* 浏览记录按日期分组 */
        $curyear = date('Y');
        $visit_list = [];
		if(!empty($visit)){
			$visit_list['flag']=1;
		}
		else{
			$visit_list['flag']=0;
		}
        foreach ($visit as $v) {
            if ($curyear == date('Y', $v['visittime'])) {
                $date = date('m月d日', $v['visittime']);
            } else {
                $date = date('Y年m月d日', $v['visittime']);
            }
            $visit_list['data'][$date][] = $v;
        }

       echo json_encode($visit_list);
    }
	
	 public function del_visit_log()
    {
        $visit_ids = isset($_POST['ids'])?$_POST['ids']:143;
        $row = M('goods_visit')->where('visit_id','IN', $visit_ids)->delete();
		$sql= M('goods_visit')->getLastSql();
		$result=array('flag'=>0,'msg'=>'','data'=>'');
        if(!$row) {
            $result['msg']='操作失败！'.$sql;
        } else {
			 $result['flag']=1;
            $result['msg']='操作成功！';
        }
		echo json_encode($result);
    }	
	
	/*
     * 用户地址列表
     */
    public function address_list()
    {
		$this->user_id=isset($_POST['user'])?intval($_POST['user']):1;
        $address_lists = get_user_address_list($this->user_id);
        $region_list = get_region_list();
		$data=array('flag'=>0,'msg'=>'',data=>array());
       if(!empty($address_lists)){
		   foreach($address_lists as $k => $v){
			  
			   $address_lists[$k]['address']=$region_list[$v['country']].$region_list[$v['province']].$region_list[$v['city']].$region_list[$v['district']].$v['address'];
		   }
		   $data['flag']=1;
		   $data['data']=$address_lists;
		   $data['region']=$region_list;//district
	   }
	   else{
		   $data['msg']='没有地址信息';
	   }
	 // var_dump( $data['region']);
	   echo json_encode($data);
    }

	 public function edit_address()
    {
		$this->user_id=isset($_POST['user'])?intval($_POST['user']):2592;
        $id=isset($_POST['id'])?$_POST['id']:828;
		
        $address_lists = M('user_address')->where(array('address_id' => $id, 'user_id' => $this->user_id))->find();
        $region_list = get_region_list();
		$data=array('flag'=>0,'msg'=>'',data=>array());
       if(!empty($address_lists)){
		
		    $address_lists['addresss']=$region_list[$address_lists['country']].$region_list[$address_lists['province']].$region_list[$address_lists['city']].$region_list[$address_lists['district']];//.$address_lists['address'];
		   
		   $data['flag']=1;
		   $data['data']=$address_lists;
	   }
	   else{
		   $data['msg']='没有地址信息';
	   }
		echo json_encode($data);
		
		/*  //获取省份
        $p = M('region')->where(array('parent_id' => 0, 'level' => 1))->select();
        $c = M('region')->where(array('parent_id' => $address['province'], 'level' => 2))->select();
        $d = M('region')->where(array('parent_id' => $address['city'], 'level' => 3))->select();
        if ($address['twon']) {
            $e = M('region')->where(array('parent_id' => $address['district'], 'level' => 4))->select();
        } */
		
		//$sans=array();
		/* foreach($c as $ck=>$cv){
				$sans[$ck]['value']=$cv['id'];
				$sans[$ck]['text']=$cv['name'];
				$d = M('region')->where(array('parent_id' => $cv['id'], 'level' => 3))->select();
				foreach($d as $k=>$v){
					$sans[$ck]['children'][$k]['value']=$v['id'];
					$sans[$ck]['children'][$k]['text']=$v['name'];
				}
				//$sans[$ck]['children']=$d;
		}  */
		/* 	$san=array();
			$p = M('region')->where(array('parent_id' => 0, 'level' => 1))->select();
			foreach($p as $k=>$v){
			$san[$k]['value']=$v['id'];
			$san[$k]['text']=$v['name'];
			$c = M('region')->where(array('parent_id' => $v['id'], 'level' => 2))->select();
			foreach($c as $ck=>$cv){
				$san[$k]['children'][$ck]['value']=$cv['id'];
				$san[$k]['children'][$ck]['text']=$cv['name'];
				$d = M('region')->where(array('parent_id' => $cv['id'], 'level' => 3))->select();
				foreach($d as $ks=>$vs){
					$san[$k]['children'][$ck]['children'][$ks]['value']=$vs['id'];
					$san[$k]['children'][$ck]['children'][$ks]['text']=$vs['name'];
				}
			}
		}
		file_put_contents('json.txt',json_encode($san));
		var_dump($san); */
    }
	
	public function saveAddress(){
		$this->user_id=isset($_POST['user'])?intval($_POST['user']):2592;
        $id=isset($_POST['id'])?$_POST['id']:828;
		$data['is_default']=isset($_POST['is_default'])?$_POST['is_default']:1;
		$data['consignee']=isset($_POST['consignee'])?trim($_POST['consignee']):1;
		$data['mobile']=isset($_POST['mobile'])?trim($_POST['mobile']):1;
		$data['address']=isset($_POST['address'])?trim($_POST['address']):1;
		$areas=isset($_POST['areas'])?$_POST['areas']:1;
		//$areas=array(1,1,1);
		$data['province']=$areas[0];
		$data['city']=$areas[1];
		$data['district']=$areas[2];
		$data['user_id']=$this->user_id;
		$flag=isset($_POST['flag'])?$_POST['flag']:1;
		$result=array('falg'=>0,'msg'=>'',data=>'');
		//echo json_encode($data);exit;
		if(empty($data['consignee'])){
			$result['msg']='收件人不能为空';
			echo json_encode($result);
			exit;
		}
		
		if(!$this->isMobile($data['mobile'])){
			$result['msg']='手机格式不对';
			echo json_encode($result);
			exit;
		}
		
		if(empty($areas)){
			$result['msg']='请选择区域';
			echo json_encode($result);
			exit;
		}
		
		if(empty($data['address'])){
			$result['msg']='详细地址不能为空';
			echo json_encode($result);
			exit;
		}
		
		if($flag==2){
			$up=M('user_address')->where(array('address_id' => $id, 'user_id' => $this->user_id))->save($data);
			if($up){
				$result['flag']=1;
				$result['msg']='更新成功';
			}
			else{
				$result['msg']='更新失败';
			}
		}
		elseif($flag==1){
			$sql='INSERT INTO `tp_user_address` (`is_default` , `consignee` , `mobile` , `address` , `province` , `city` , `district` , `user_id`) VALUES (';
			$in=M('user_address')->add($data);
			//echo M('user_address')->getLastSql();
			if($in){
				$result['flag']=1;
				$result['msg']='添加成功';
			}
			else{
				$result['msg']='添加失败';
			}
		}
		
		echo json_encode($result);
	}
	
	function getGoodsinfo(){
		$data=array('flag'=>0,'msg'=>'','data'=>array());
		$goods_id = isset($_POST['id'])?intval($_POST['id']):0;
		$goodsLogic = new \app\home\logic\GoodsLogic();
        $goodsModel = new \app\home\model\Goods();
		$user_id=isset($_POST['user_id'])?$_POST['user_id']:0;
		
        $goods = $goodsModel::get($goods_id);
        $goods['discount'] = $goods->discount;
        $goodsPromFactory = new GoodsPromFactory();
        if ($goodsPromFactory->checkPromType($goods['prom_type'])) {
            $goodsPromLogic = $goodsPromFactory->makeModule($goods['prom_type'],$goods['prom_id']);//这里会自动更新商品活动状态，所以商品需要重新查询
            $goods = M('Goods')->where("goods_id", $goods_id)->find();
            //商品活动类型为秒杀
            if($goods['prom_type'] == 1 && $goodsPromLogic->checkActivityIsAble()){
                $goods['flash_sale'] = $goodsPromLogic->getPromModel();
                $goods['discount'] = round($goods['flash_sale']['price']/$goods['shop_price'],2)*10;
            }
        }
		//已下架商品
        if(empty($goods) || ($goods['is_on_sale'] == 0)){
            $data['msg']='此商品不存在或者已下架';
        }
		else{
			$data['flag']=1;
		}
        if ($user_id) {
            $goodsLogic->add_visit_log($user_id, $goods);//访问历史
        }
		//品牌
        if($goods['brand_id']){
            $brnad = M('brand')->where("id", $goods['brand_id'])->find();
            $goods['brand_name'] = $brnad['name'];
        }
		$goods['sale_num'] = M('order_goods')->where(['goods_id'=>$goods_id,'is_send'=>1])->count();//销售量
        $goods_images_list = M('GoodsImages')->where("goods_id", $goods_id)->select();
		$filter_spec = $goodsLogic->get_spec($goods_id);//规格
        $spec_goods_price  = M('spec_goods_price')->where("goods_id", $goods_id)->getField("key,price,store_count"); // 规格 对应 价格 库存表
        
		$goods_attribute = M('GoodsAttribute')->getField('attr_id,attr_name'); // 查询属性
        $goods_attr_list = M('GoodsAttr')->where("goods_id", $goods_id)->select(); // 查询商品属性表
		$commentStatistics = $goodsLogic->commentStatistics($goods_id);// 获取某个商品的评论统计
        //cart goods number
		$sql='select goods_num from tp_cart where user_id = '.$user_id.' and goods_id='.$goods_id;
		$count=M('cart')->query($sql);
		if(empty($count)){
			$count=0;
		}
		$data['spec']=$filter_spec;
		$data['goods_spec_price']=$spec_goods_price;
		$data['img_list']=$goods_images_list;
		$data['imgcount']=count($goods_images_list);
		
		$data['attr']=$goods_attribute;
		$data['attr_list']=$goods_attr_list;
		$goods['goods_content']= html_entity_decode($goods['goods_content']);
		$data['data']=$goods;
		$data['cartnumber']=$count[0]['goods_num'];
		echo json_encode($data);
		exit;
         // 商品 图册
        $goods_attribute = M('GoodsAttribute')->getField('attr_id,attr_name'); // 查询属性
        $goods_attr_list = M('GoodsAttr')->where("goods_id", $goods_id)->select(); // 查询商品属性表
		$commentStatistics = $goodsLogic->commentStatistics($goods_id);// 获取某个商品的评论统计
        //$this->assign('spec_goods_price', json_encode($spec_goods_price,true)); // 规格 对应 价格 库存表
      	//当前用户收藏
        $user_id = cookie('user_id');
        $collect = M('goods_collect')->where(array("goods_id"=>$goods_id ,"user_id"=>$user_id))->count();
        $goods_collect_count = M('goods_collect')->where(array("goods_id"=>$goods_id))->count(); //商品收藏数
        var_dump($goods);
	}
	
	function collectGoods(){
		$goods_id = isset($_POST['id'])?intval($_POST['id']):0;
		$user_id=isset($_POST['user_id'])?$_POST['user_id']:0;
        $goodsLogic = new \app\home\logic\GoodsLogic();
        $result = $goodsLogic->collect_goods($user_id,$goods_id);
        exit(json_encode($result));
	}
	
	function addCart(){
		$goods_id = isset($_POST['id'])?$_POST['id']:0; // 商品id
        $goods_num = isset($_POST['number'])?$_POST['number']:0;// 商品数量
        $goods_spec = isset($_POST['goods_spec'])?$_POST['goods_spec']:array(); // 商品规格
		$data=array('status'=>0,'msg'=>'','result'=>'','count'=>0);
		$user_id=isset($_POST['user_id'])?$_POST['user_id']:2592;
		
		$user = M('users')->where("user_id", $user_id)->find();
            session('user', $user);  //覆盖session 中的 user
            $this->user = $user;
            $this->user_id = $user['user_id'];
           // $this->assign('user', $user); //存储用户信息
            // 给用户计算会员价 登录前后不一样
            if ($user) {
                $user['discount'] = (empty($user['discount'])) ? 1 : $user['discount'];
                if ($user['discount'] != 1) {
                    $c = Db::name('cart')->where(['user_id' => $user['user_id'], 'prom_type' => 0])->where('member_goods_price = goods_price')->count();
                    $c && Db::name('cart')->where(['user_id' => $user['user_id'], 'prom_type' => 0])->update(['member_goods_price' => ['exp', 'goods_price*' . $user['discount']]]);
                }
            }
		
		
        if(empty($goods_id)){
            $data['msg']='请选择要购买的商品'.$goods_id;
			 exit(json_encode($data));
        }
		elseif(empty($goods_num)){
              $data['msg']='购买商品数量不能为0';
			   exit(json_encode($data));
        }
        $spec_key = array_values($goods_spec);
        if($spec_key){
            sort($spec_key);
            $goods_spec_key = implode('_', $spec_key);
        }else{
            $goods_spec_key = '';
        }
        $this->cartLogic->setGoodsModel($goods_id);
        $this->cartLogic->setUserId($this->user_id);
        $result = $this->cartLogic->addGoodsToCart($goods_num, $goods_spec_key);
		$sql='select goods_num from tp_cart where user_id = '.$user_id.' and goods_id='.$goods_id;
		$count=M('cart')->query($sql);
		//var_dump($count);exit;
		$result['count']=$count[0]['goods_num'];
		$data=$result;
		// 将商品加入购物车
//        $result = $this->cartLogic->addCart($goods_id, $goods_num, $goods_spec,$this->session_id,$this->user_id); // 将商品加入购物车
        exit(json_encode($data));
	}
	
	
	
	 public function cancel_collect()
		{
			$id = isset($_POST['id'])?$_POST['id']:0;//I('collect_id/d');
			$this->user_id= isset($_POST['user_id'])?$_POST['user_id']:0;
			$user_id = $this->user_id;
			$data=array('flag'=>0,'msg'=>'');
			//$sql='delete from tp_goods_collect where collect_id in('.$id.') and user_id='.$user_id;
			if (M('goods_collect')->where('collect_id', 'in', $id)->delete()) {//, 'user_id' => $user_id]
				$data['msg']='取消收藏成功';//$this->success("取消收藏成功", U('User/collect_list'));
			} else {
				 $data['msg']='取消收藏失败';
			   // $this->error("取消收藏失败", U('User/collect_list'));
			}
			exit(json_encode($data));
		}
		
	public function collectcount(){
		$this->user_id= isset($_POST['user_id'])?$_POST['user_id']:2592;
		$user_id = $this->user_id;
		$count=M('goods_collect')->where('user_id='.$user_id)->count();
		exit(json_encode($count));
	}
	
	
	
	
	
		
}



















?>