<?php
namespace app\Api\controller; 
use app\admin\model\GroupBuy;
use app\home\logic\CartLogic;
use app\home\logic\OrderLogic;
use think\Db;
use app\home\logic\GoodsLogic;


header('content-type:application:json;charset=utf8');  
header('Access-Control-Allow-Origin:*');  
header('Access-Control-Allow-Methods:POST');  
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE');


class Cart{
    
    public $cartLogic; // 购物车逻辑操作类    
    public $user_id = 0;
    public $user = array();    
	public function  __construct() {
       // parent::__construct();
        $this->cartLogic = new \app\home\logic\CartLogic();
	 }
	function addCart(){
		$goods_id = isset($_POST['id'])?$_POST['id']:0; // 商品id
        $goods_num = isset($_POST['number'])?$_POST['number']:0;// 商品数量
        $goods_spec = isset($_POST['goods_spec'])?$_POST['goods_spec']:array(); // 商品规格
		$data=array();
		$user_id=isset($_POST['user_id'])?$_POST['user_id']:0;
		
		$user = M('users')->where("user_id", $user_id)->find();
            session('user', $user);  //覆盖session 中的 user
            $this->user = $user;
            $this->user_id = $user['user_id'];
           // $this->assign('user', $user); //存储用户信息
            // 给用户计算会员价 登录前后不一样
            if ($user) {
                $user['discount'] = (empty($user['discount'])) ? 1 : $user['discount'];
                if ($user['discount'] != 1) {
                    $c = Db::name('cart')->where(['user_id' => $user['user_id'], 'prom_type' => 0])->where('member_goods_price = goods_price')->count();
                    $c && Db::name('cart')->where(['user_id' => $user['user_id'], 'prom_type' => 0])->update(['member_goods_price' => ['exp', 'goods_price*' . $user['discount']]]);
                }
            }
		
		
        if(empty($goods_id)){
            $data=array(['status'=>0,'msg'=>'请选择要购买的商品','result'=>'']);
        }
        if(empty($goods_num)){
             $data=array(['status'=>0,'msg'=>'购买商品数量不能为0','result'=>'']);
        }
        $spec_key = array_values($goods_spec);
        if($spec_key){
            sort($spec_key);
            $goods_spec_key = implode('_', $spec_key);
        }else{
            $goods_spec_key = '';
        }
        $this->cartLogic->setGoodsModel($goods_id);
        $this->cartLogic->setUserId($this->user_id);
        $result = $this->cartLogic->addGoodsToCart($goods_num, $goods_spec_key); // 将商品加入购物车
//        $result = $this->cartLogic->addCart($goods_id, $goods_num, $goods_spec,$this->session_id,$this->user_id); // 将商品加入购物车
        exit(json_encode($result));
	}
	
	 public function ajaxCartList()
    {
        $post_goods_num = isset($_POST['number'])?intval($_POST['number']):0;//I("goods_num/a"); // goods_num 购物车商品数量
        $post_cart_select = isset($_POST['select'])?intval($_POST['select']):1;//I("cart_select/a"); // 购物车选中状态
        $this->user_id=isset($_POST['user_id'])?intval($_POST['user_id']):2592;
		$goodsLogic = new GoodsLogic();
        //$where['session_id'] = $this->session_id; // 默认按照 session_id 查询
        //如果这个用户已经登录则按照用户id查询
		$where=array();
        if ($this->user_id) {
           // unset($where);
            $where['user_id'] = $this->user_id;
        }
		$sql='select c.*,g.original_img from tp_cart as c left join tp_goods as g on c.goods_id=g.goods_id where user_id='.$this->user_id;
        $cartList =M('Cart')->query($sql);
//		M('Cart')->where($where)->getField("id,goods_num,selected,prom_type,prom_id,goods_id,goods_price,spec_key");
       /*  if ($post_goods_num) {
            // 修改购物车数量 和勾选状态
            foreach ($post_goods_num as $key => $val) {
                $data['goods_num'] = $val < 1 ? 1 : $val;
                $data['selected'] = $post_cart_select[$key] ? 1 : 0;
                //普通商品
                if($cartList[$key]['prom_type'] == 0 && (empty($cartList[$key]['spec_key']))){
                    $goods = Db::name('goods')->where('goods_id', $cartList[$key]['goods_id'])->find();
                    // 如果有阶梯价格
                    if (!empty($goods['price_ladder'])) {
                        $price_ladder = unserialize($goods['price_ladder']);
                        $data['member_goods_price'] = $data['goods_price'] = $goodsLogic->getGoodsPriceByLadder($data['goods_num'], $cartList[$key]['goods_price'], $price_ladder);
                    }
                }
                //限时抢购 不能超过购买数量
                if ($cartList[$key]['prom_type'] == 1) {
                    $FlashSaleLogic = new \app\admin\logic\FlashSaleLogic($cartList[$key]['prom_id']);
                    $data['goods_num'] = $FlashSaleLogic->getUserFlashResidueGoodsNum($this->user_id,$data['goods_num']); //获取用户剩余抢购商品数量
                }
                //团购 不能超过购买数量
                if($cartList[$key]['prom_type'] == 2){
                    $groupBuyLogic =  new \app\admin\logic\GroupBuyLogic($cartList[$key]['prom_id']);
                    $groupBuySurplus = $groupBuyLogic->getPromotionSurplus();//团购剩余库存
                    if($data['goods_num'] > $groupBuySurplus){
                        $data['goods_num'] = $groupBuySurplus;
                    }
                }
                if (($cartList[$key]['goods_num'] != $data['goods_num']) || ($cartList[$key]['selected'] != $data['selected'])){
                    M('Cart')->where("id", $key)->save($data);
                }
            }
           // $this->assign('select_all', input('post.select_all')); // 全选框
        } */
        $cartLogic = new CartLogic();
        $cartLogic->setUserId($this->user_id);
        $result = $cartLogic->getUserCartList(1);
		if(!empty($cartList)){
			$result['flag']=1;
		}
		else{
			$result['flag']=0;
		}
		$result['cartList']=$cartList;
//        $result = $this->cartLogic->cartList($this->user, $this->session_id,1);
        if(empty($result['total_price'])){
            $result['total_price'] = array( 'total_fee' =>0, 'cut_fee' =>0, 'num' => 0, 'atotal_fee' =>0, 'acut_fee' =>0, 'anum' => 0);
        }
		exit(json_encode($result));
       
    }
	
	
	 public function ajaxDelCart()
		{
			$ids = isset($_POST['id'])?intval($_POST['id']):0; // 商品 ids
			$result = M("Cart")->where("id","in",$ids)->delete(); // 删除id为5的用户数据
			if(!empty($result)){
				$return_arr = array('status'=>1,'msg'=>'删除成功','result'=>''); // 返回结果状态
			
			}
			else{
				$return_arr = array('status'=>0,'msg'=>'删除失败','result'=>''); // 返回结果状态
			
			}
			exit(json_encode($return_arr));
		}
		
	public function updateCart(){
		$id=isset($_POST['id'])?intval($_POST['id']):17;
		$number=isset($_POST['number'])?intval($_POST['number']):12;
		$user_id=isset($_POST['user_id'])?intval($_POST['user_id']):2592;
		$selected=isset($_POST['selected'])?intval($_POST['selected']):1;
		$data=array('flag'=>0,'msg'=>'');
		if(!empty($id) && !empty($number) && !empty($user_id)){
			$datas['id']=$id;
			$datas['number']=$number;
			$datas['user_id']=$user_id;
			$sql='update tp_cart set goods_num='.$number.',selected='.$selected.' where id='.$id.' and user_id='.$user_id;
			$rs=M('Cart')->query($sql);//where('id='.$id)->save($datas);
			
			$data['flag']=1;
			$data['msg']='修改成功';
			
		}
		else{
			$data['msg']='参数错误';
		}
		exit(json_encode($data));
	}
	
	
	 public function submitorder()
    {
        $address_id = isset($_POST['aid'])?intval($_POST['aid']):828;//I('address_id/d');
		$this->user_id=isset($_POST['user_id'])?intval($_POST['user_id']):2592;
		$shipping_code='';
        $cartLogic = new CartLogic();
		$data=array('flag'=>0,'msg'=>'');
        $cid = isset($_POST['cid'])?intval($_POST['cid']):0;//I('cid/d');
        if($this->user_id == 0){
			$data['flag']=2;
            $data['msg']='请先登陆';
			exit(json_encode($data));
        }
        if($address_id){
            $address = M('user_address')->where("address_id", $address_id)->find();
        } else {
            $address = M('user_address')->where(['user_id'=>$this->user_id,'is_default'=>1])->find();
        }
        if(empty($address)){
        	$data['msg']='请填写收货人地址信息';
			exit(json_encode($data));
        }else{
			$data['address']=$address;
        	//$this->assign('address',$address);
        }
        $cartLogic->setUserId($this->user_id);
        if($cartLogic->getUserCartOrderCount() == 0){
            $data['msg']='你的购物车没有选中商品';
			exit(json_encode($data));
			//$this->error ('你的购物车没有选中商品','Cart/cart');
        }
        $result =$cartLogic->getUserCartList(1); // 获取购物车商品

        // 找出这个用户的优惠券 没过期的  并且 订单金额达到 condition 优惠券指定标准的
        $couponWhere = [
            'c2.uid' => $this->user_id,
            'c1.use_end_time' => ['gt', time()],
            'c1.use_start_time' => ['lt', time()],
            'c1.condition' => ['elt', $result['total_price']['total_fee']]
        ];
        $couponList = Db::name('coupon')->alias('c1')
            ->field('c1.name,c1.money,c1.condition,c2.*')
            ->join('__COUPON_LIST__ c2', ' c2.cid = c1.id and c1.type in(0,1,2,3) and order_id = 0', 'inner')
            ->where($couponWhere)
            ->select();
        if(!empty($cid)){
            $checkconpon = M('coupon')->field('id,name,money')->where("id", $cid)->find();    //要使用的优惠券
            $checkconpon['lid'] = isset($_POST['lid'])?intval($_POST['lid']):0;//I('lid/d');
        }
        
         $shippingList = M('Plugin')->where("`type` = 'shipping' and status = 1")->cache(true,TPSHOP_CACHE_TIME)->select();// 物流公司            
       // var_dump($shippingList);
		foreach($shippingList as $k => $v) {
            $dispatchs = calculate_price($this->user_id, $result['cartList'], 0, $v['code'], $address['province'], $address['city'], $address['district']);
            if($k==0){
				$shipping_code=$v['code'];
			}
			if ($dispatchs['status'] !== 1) {
               $data['msg']=$dispatchs['msg'];//'物流配置有问题';
				exit(json_encode($data));
			//$this->error('物流配置有问题');
            }
            $shippingList[$k]['freight'] = $dispatchs['result']['shipping_price'];
        }
		$sql='select c.*,g.original_img from tp_cart as c left join tp_goods as g on c.goods_id=g.goods_id where user_id='.$this->user_id;
        $cartList =M('Cart')->query($sql);
		$data['flag']=1;
        $data['couponList']=$couponList;
		$data['shippingList']=$shippingList;
		$data['cartList']=$cartList;//$result['cartList'];
		$data['total_price']=$result['total_price'];
		$data['checkconpon']=$checkconpon;
		exit(json_encode($data));
    }
	/**
     * ajax 获取订单商品价格 或者提交 订单
     */
    public function getOrderPay(){
		$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:2592;
        if($this->user_id == 0){
            exit(json_encode(array('status'=>-100,'msg'=>"登录超时请重新登录!",'result'=>null))); // 返回结果状态
        }
        $address_id =isset($_POST['address_id'])?$_POST['address_id']:828;//ddress_id/d"); //  收货地址id
        $shipping_code =isset($_POST['"shipping_code'])?$_POST['"shipping_code']:'shunfeng';//ing_code"); //  物流编号        
        $invoice_title =isset($_POST['invoice_title'])?$_POST['invoice_title']:0;// I('invoice_title'); // 发票
        $coupon_id =isset($_POST['coupon_id'])?$_POST['coupon_id']:0;//  I("coupon_id/d"); //  优惠券id
        $couponCode = 0; //I("couponCode"); //  优惠券代码
        $pay_points = isset($_POST['pay_points'])?$_POST['pay_points']:0;// I("pay_points/d",0); //  使用积分
        $user_money = isset($_POST['user_money'])?$_POST['user_money']:0;// I("user_money/f",0); //  使用余额
        $user_note =isset($_POST['user_desc'])?$_POST['user_desc']:0;// trim(I('user_note'));   //买家留言
        $paypwd = isset($_POST['paypwd'])?$_POST['paypwd']:0;// I("paypwd",''); // 支付密码
        
        $user_money = $user_money ? $user_money : 0;
        $cartLogic = new CartLogic();
        $cartLogic->setUserId($this->user_id);
        if($cartLogic->getUserCartOrderCount() == 0 ) {
            exit(json_encode(array('status'=>-2,'msg'=>'你的购物车没有选中商品','result'=>null))); // 返回结果状态
        }
        if(!$address_id) exit(json_encode(array('status'=>-3,'msg'=>'请先填写收货人信息','result'=>null))); // 返回结果状态
        if(!$shipping_code) exit(json_encode(array('status'=>-4,'msg'=>'请选择物流信息','result'=>null))); // 返回结果状态
		
		$address = M('UserAddress')->where("address_id", $address_id)->find();
		$order_goods = M('cart')->where(['user_id'=>$this->user_id,'selected'=>1])->select();
        $result = calculate_price($this->user_id,$order_goods,$shipping_code,0,$address['province'],$address['city'],$address['district'],$pay_points,$user_money,$coupon_id,$couponCode);
                
		if($result['status'] < 0)	
			exit(json_encode($result));      	
	// 订单满额优惠活动		                
        $order_prom = get_order_promotion($result['result']['order_amount']);
        $result['result']['order_amount'] = $order_prom['order_amount'] ;
        $result['result']['order_prom_id'] = $order_prom['order_prom_id'] ;
        $result['result']['order_prom_amount'] = $order_prom['order_prom_amount'] ;
			
        $car_price = array(
            'postFee'      => $result['result']['shipping_price'], // 物流费
            'couponFee'    => $result['result']['coupon_price'], // 优惠券            
            'balance'      => $result['result']['user_money'], // 使用用户余额
            'pointsFee'    => $result['result']['integral_money'], // 积分支付
            'payables'     => $result['result']['order_amount'], // 应付金额
            'goodsFee'     => $result['result']['goods_price'],// 商品价格
            'order_prom_id' => $result['result']['order_prom_id'], // 订单优惠活动id
            'order_prom_amount' => $result['result']['order_prom_amount'], // 订单优惠活动优惠了多少钱            
        );
		if($_REQUEST['act'] == 'submit_order') {
            $pay_name = '';
            if ($pay_points || $user_money) {
                if ($this->user['is_lock'] == 1) {
                    exit(json_encode(array('status'=>-5,'msg'=>"账号异常已被锁定，不能使用余额支付！",'result'=>null))); // 用户被冻结不能使用余额支付
                }
                if (empty($this->user['paypwd'])) {
                    exit(json_encode(array('status'=>-6,'msg'=>'请先设置支付密码','result'=>null)));
                }
                if (empty($paypwd)) {
                    exit(json_encode(array('status'=>-7,'msg'=>'请输入支付密码','result'=>null)));
                }
                if (encrypt($paypwd) !== $this->user['paypwd']) {
                    exit(json_encode(array('status'=>-8,'msg'=>'支付密码错误','result'=>null)));
                }
                $pay_name = $user_money ? '余额支付' : '积分兑换';
            }
            if(empty($coupon_id) && !empty($couponCode)){
                $coupon_id = M('CouponList')->where("code", $couponCode)->getField('id');
            }
            $orderLogic = new OrderLogic();
            $result = $orderLogic->addOrder($this->user_id,$address_id,$shipping_code,$invoice_title,$coupon_id,$car_price,$user_note,$pay_name); // 添加订单
			$result['flag']=1;
		   exit(json_encode($result));
        }
            $return_arr = array('status'=>1,'flag'=>0,'msg'=>'计算成功','result'=>$car_price); // 返回结果状态
            exit(json_encode($return_arr));
    }
	
	
	public function pay(){
        $order_id = isset($_POST['order_id'])?intval($_POST['order_id']):1513;//I('order_id/d');
        $order = M('Order')->where("order_id", $order_id)->find();
        // 如果已经支付过的订单直接到订单详情页面. 不再进入支付页面
		$data=array('flag'=>0,'msg'=>'','data'=>'');
        if($order['pay_status'] == 1){
            $data['flag']=2;
			$data['msg']='订单已支付';
			exit(json_encode($data));
        }
		
        $payment_where['type'] = 'payment';
        if(strstr($_SERVER['HTTP_USER_AGENT'],'MicroMessenger')){
            //微信浏览器
            if($order['order_prom_type'] == 4 || $order['order_prom_type'] == 1){
                //预售订单和抢购不支持货到付款
                $payment_where['code'] = 'weixin';
            }else{
                $payment_where['code'] = array('in',array('weixin','cod'));
            }
        }else{
            if($order['order_prom_type'] == 4 || $order['order_prom_type'] == 1){
                //预售订单和抢购不支持货到付款
                $payment_where['code'] = array('neq','cod');
            }
            $payment_where['scene'] = array('in',array('0','1'));
        }
        if($order['order_prom_type'] != 4){
			
            $userlogic = new \app\home\logic\UsersLogic();
            $res = $userlogic->abolishOrder($order['user_id'],$order['order_id'],$order['add_time']);  //检测是否超时没支付
            if($res['status']==1)
			{
				$data['msg']='订单超时未支付已自动取消';
				exit(json_encode($data));
			}
			
        }

        $payment_where['status'] = 1;
        //预售和抢购暂不支持货到付款
        $orderGoodsPromType = M('order_goods')->where(['order_id'=>$order['order_id']])->getField('prom_type',true);
        if($order['order_prom_type'] == 4 || in_array(1,$orderGoodsPromType)){
            $payment_where['code'] = array('neq','cod');
        }
        $paymentList = M('Plugin')->where($payment_where)->select();
        $paymentList = convert_arr_key($paymentList, 'code');

        foreach($paymentList as $key => $val)
        {
            $val['config_value'] = unserialize($val['config_value']);
            if($val['config_value']['is_bank'] == 2)
            {
                $bankCodeList[$val['code']] = unserialize($val['bank_code']);
            }
            //判断当前浏览器显示支付方式
            if(($key == 'weixin' && !is_weixin()) || ($key == 'alipayMobile' && is_weixin())){
                unset($paymentList[$key]);
            }
        }

        $bank_img = include APP_PATH.'home/bank.php'; // 银行对应图片
        $payment = M('Plugin')->where("`type`='payment' and status = 1")->select();
		$data['flag']=1;
		$data['paymentList']=$paymentList;
		$data['bank_img']=$bank_img;
		$data['order']=$order;
		$data['bankCodeList']=$bankCodeList;
		$data['pay_date']=date('Y-m-d', strtotime("+1 day"));
		exit(json_encode($data));
		
		/* 
        $this->assign('paymentList',$paymentList);
        $this->assign('bank_img',$bank_img);
        $this->assign('order',$order);
        $this->assign('bankCodeList',$bankCodeList);
        $this->assign('pay_date',date('Y-m-d', strtotime("+1 day")));
        return $this->fetch(); */
    }
	
	function payMsg(){
			$order_id = isset($_POST['order_id'])?$_POST['order_id']:0;//I('order_id/d'); // 订单id
			$this->pay_code = isset($_POST['pay_radio'])?$_POST['pay_radio']:0;
			if(!$this->pay_code){
				$this->pay_code = isset($_POST['pay_code'])?$_POST['pay_code']:0;
			}
			
			
			
			$data=array('flag'=>0,'msg'=>'','data'=>'');
			if(empty($order_id) || empty($this->pay_code)){
				$data['msg']='订单支付失败！';
				exit(json_encode($data));
			}
            // 修改订单的支付方式
            $payment_arr = M('Plugin')->where("`type` = 'payment'")->getField("code,name");                        
            M('order')->where("order_id", $order_id)->save(array('pay_code'=>$this->pay_code,'pay_name'=>$payment_arr[$this->pay_code]));
            $order = M('order')->where("order_id", $order_id)->find();
            if($order['pay_status'] == 1){
            	$data['msg']='此订单，已完成支付!';
				exit(json_encode($data));
            }
            if($order['pay_code']==$this->pay_code){
				$data['flag']=1;
				$data['msg']='订单支付成功，你选择的是货到付款支付方式，请在收到货品货，支付商品金额！';
				exit(json_encode($data));
			}
			else{
				$data['msg']='订单支付失败！';
				exit(json_encode($data));
			}
	}
	
	/* 
	public function getCartGoodsTotalPric(){
		$this->user_id=2592;
		$order_goods = M('cart')->where(['user_id'=>$this->user_id,'selected'=>1])->select();
        if (empty($order_goods)){
			return array('status' => -9, 'msg' => '商品列表不能为空', 'result' => '');
		}
		$goods_id_arr = get_arr_column($order_goods, 'goods_id');
		$goods_arr = M('goods')->where("goods_id in(" . implode(',', $goods_id_arr) . ")")->cache(true,TPSHOP_CACHE_TIME)->getField('goods_id,weight,market_price,is_free_shipping,exchange_integral,shop_price'); // 商品id 和重量对应的键值对
		foreach ($order_goods as $key => $val) {
			// 如果传递过来的商品列表没有定义会员价
			if (!array_key_exists('member_goods_price', $val)) {
				$user['discount'] = $user['discount'] ? $user['discount'] : 1; // 会员折扣 不能为 0
				$order_goods[$key]['member_goods_price'] = $val['member_goods_price'] = $val['goods_price'] * $user['discount'];
			}
			//如果商品不是包邮的
			if ($goods_arr[$val['goods_id']]['is_free_shipping'] == 0)
				$goods_weight += $goods_arr[$val['goods_id']]['weight'] * $val['goods_num']; //累积商品重量 每种商品的重量 * 数量
			
			//计算订单可用积分
			if($goods_arr[$val['goods_id']]['exchange_integral']>0){
				//商品设置了积分兑换就用商品本身的积分。
				$result['order_integral'] +=  $goods_arr[$val['goods_id']]['exchange_integral'];
			}else{
				//没有就按照会员价与平台设置的比例来计算。
				$result['order_integral'] +=  ceil($order_goods[$key]['member_goods_price'] * $use_percent_point);
			}
			$order_goods[$key]['goods_fee'] = $val['goods_num'] * $val['member_goods_price'];    // 小计
			$order_goods[$key]['store_count'] = getGoodNum($val['goods_id'], $val['spec_key']); // 最多可购买的库存数量
			if ($order_goods[$key]['store_count'] <= 0)
				return array('status' => -10, 'msg' => $order_goods[$key]['goods_name'] . "库存不足,请重新下单", 'result' => '');

			$goods_price += $order_goods[$key]['goods_fee']; // 商品总价
			$cut_fee += $val['goods_num'] * $val['market_price'] - $val['goods_num'] * $val['member_goods_price']; // 共节约
			$anum += $val['goods_num']; // 购买数量
		}
	
	} */
	/* 
	public function userPointsForGood($pay_points){
		$use_percent_point = tpCache('shopping.point_use_percent') / 100;     //最大使用限制: 最大使用积分比例, 例如: 为50时, 未50% , 那么积分支付抵扣金额不能超过应付金额的50%
		
		if(($pay_points > 0 && $use_percent_point == 0) ||  ($pay_points >0 && $result['order_integral']==0)){
			return array('status' => -1, 'msg' => "该笔订单不能使用积分", 'result' => '积分'); // 返回结果状态
		}

		if ($pay_points && ($pay_points > $user['pay_points']))
			return array('status' => -5, 'msg' => "你的账户可用积分为:" . $user['pay_points'], 'result' => ''); // 返回结果状态
		
		//能否使用积分
		 //.积分低于point_min_limit时,不可使用
		//.在不使用积分的情况下, 计算商品应付金额
		//.原则上, 积分支付不能超过商品应付金额的50%, 该值可在平台设置
		 //
		$point_rate = tpCache('shopping.point_rate'); //兑换比例: 如果拥有的积分小于该值, 不可使用
		$min_use_limit_point = tpCache('shopping.point_min_limit'); //最低使用额度: 如果拥有的积分小于该值, 不可使用


		if ($min_use_limit_point > 0 && $pay_points > 0 && $pay_points < $min_use_limit_point) {
			return array('status' => -1, 'msg' => "您使用的积分必须大于{$min_use_limit_point}才可以使用", 'result' => ''); // 返回结果状态
		}
		// 计算该笔订单最多使用多少积分
		if(($use_percent_point !=1 ) && $pay_points > $result['order_integral']) {
		   return array('status'=>-1,'msg'=>"该笔订单, 您使用的积分不能大于{$result['order_integral']}",'result'=>'积分'); // 返回结果状态
		}


		$pay_points = ($pay_points / tpCache('shopping.point_rate')); // 积分支付 100 积分等于 1块钱
		$pay_points = ($pay_points > $order_amount) ? $order_amount : $pay_points; // 假设应付 1块钱 而用户输入了 200 积分 2块钱, 那么就让 $pay_points = 1块钱 等同于强制让用户输入1块钱
		$order_amount = $order_amount - $pay_points; //  积分抵消应付金额
		
		
	}
	
	public function cartShippingFee($goods_price){
		// 处理物流
		if ($shipping_price == 0) {
			$freight_free = tpCache('shopping.freight_free'); // 全场满多少免运费
			if ($freight_free > 0 && $goods_price >= $freight_free) {
				$shipping_price = 0;
			} else {
				$shipping_price = $goodsLogic->getFreight($shipping_code, $province, $city, $district, $goods_weight);
			}
		}
		
		
		
	}
	public function cartCoupon(){
		// 优惠券处理操作
		$coupon_price = 0;
		if ($coupon_id && $user_id) {
			$coupon_price = $couponLogic->getCouponMoney($user_id, $coupon_id); // 下拉框方式选择优惠券
		}
	} */
	
	/**
	 * 查看订单是否满足条件参加活动
	 * @param $order_amount
	 * @return array
	 */
	/* function get_order_promotion($order_amount)
	{
	//    $parse_type = array('0'=>'满额打折','1'=>'满额优惠金额','2'=>'满额送倍数积分','3'=>'满额送优惠券','4'=>'满额免运费');
		$now = time();
		$prom = M('prom_order')->where("type<2 and end_time>$now and start_time<$now and money<=$order_amount")->order('money desc')->find();
		$res = array('order_amount' => $order_amount, 'order_prom_id' => 0, 'order_prom_amount' => 0);
		if ($prom) {
			if ($prom['type'] == 0) {
				$res['order_amount'] = round($order_amount * $prom['expression'] / 100, 2);//满额打折
				$res['order_prom_amount'] = $order_amount - $res['order_amount'];
				$res['order_prom_id'] = $prom['id'];
			} elseif ($prom['type'] == 1) {
				$res['order_amount'] = $order_amount - $prom['expression'];//满额优惠金额
				$res['order_prom_amount'] = $prom['expression'];
				$res['order_prom_id'] = $prom['id'];
			}
		}
		return $res;
	} */
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}




















?>