<?php
namespace app\Api\controller;

use app\home\logic\UsersLogic;
use app\home\model\Message;
use app\common\logic\OrderLogic;
use think\Page;
use think\Request;
use think\Verify;
use think\db;


header('content-type:application:json;charset=utf8');  
header('Access-Control-Allow-Origin:*');  
header('Access-Control-Allow-Methods:POST');  
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE');

class User{
	/*
     * 订单列表
     */
    public function order_list()
		{
			$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:2592;
			$where = ' user_id=' . $this->user_id;
			$type=isset($_POST['type'])?$_POST['type']:0;
			$datas=array('flag'=>0,'msg'=>'','data'=>array());
			//echo C(strtoupper('WAITPAY'));
			//条件搜索
		    if($type){
				$where.=C(strtoupper($type));
			}
		   
			$count = M('order')->where($where)->count();
			$Page = new Page($count, 10);
			//$show = $Page->show();
			$order_str = "order_id DESC";
			$order_list = M('order')->order($order_str)->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->select();

			//获取订单商品
			$model = new UsersLogic();
			foreach ($order_list as $k => $v) {
				$order_list[$k] = set_btn_order_status($v);  // 添加属性  包括按钮显示属性 和 订单状态显示属性
				//$order_list[$k]['total_fee'] = $v['goods_amount'] + $v['shipping_fee'] - $v['integral_money'] -$v['bonus'] - $v['discount']; //订单总额
				$data = $model->get_order_goods($v['order_id']);
				$order_list[$k]['goods_list'] = $data['result'];
			}
			//统计订单商品数量
			foreach ($order_list as $key => $value) {
				$count_goods_num = '';
				foreach ($value['goods_list'] as $kk => $vv) {
					$sql='select original_img from tp_goods where goods_id='.$value['goods_list'][$kk]['goods_id'];
					$oimg=M('goods')->query($sql);
					$order_list[$key]['goods_list'][$kk]['img']=$oimg[0]['original_img'];
					$count_goods_num += $vv['goods_num'];
				}
				$order_list[$key]['count_goods_num'] = $count_goods_num;
			}
			if(!empty($order_list)){
				$datas['flag']=1;
			}
			
			$datas['order_status']=C('ORDER_STATUS');
			$datas['shipping_status']=C('SHIPPING_STATUS');
			$datas['pay_status']=C('PAY_STATUS');
			//$data['page']=$show;
			$datas['lists']=$order_list;
			$datas['active']='order_list';
			$datas['active_status']=$type;
			exit(json_encode($datas));
			
		}
		
	public function waitPayCount(){
			$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:2592;
			$where = ' user_id=' . $this->user_id;
			$type=isset($_POST['type'])?$_POST['type']:'WAITPAY';
			$datas=array('flag'=>0,'msg'=>'','data'=>array());
			//echo C(strtoupper('WAITPAY'));
			//条件搜索WAITSEND
		    if($type){
				$where.=C(strtoupper($type));
			}
		   $data='';
			$count = M('order')->where($where)->count();
			$data['waitpay']=$count;
			$where = ' user_id=' . $this->user_id;
			$where.=C(strtoupper('WAITSEND'));
			$count = M('order')->where($where)->count();
			$data['waitsend']=$count;
			
			exit(json_encode($data));
	}
		
	 public function cancel_order()
		{
			$id = isset($_POST['id'])?intval($_POST['id']):0;//I('get.id/d');
			$this->user_id=isset($_POST['user_id'])?intval($_POST['user_id']):0;
			//检查是否有积分，余额支付
			$logic = new UsersLogic();
			$data = $logic->cancel_order($this->user_id, $id);
			exit(json_encode($data));
		}	
		
	 public function order_detail()
    {
        $id =isset($_POST['order_id'])?$_POST['order_id']:1514;// I('get.id/d');
        $this->user_id=isset($_POST['user_id'])?intval($_POST['user_id']):2592;
		$datas=array('flag'=>0,'msg'=>'','data'=>array());
		if(empty($id) || empty($this->user_id)){
			$datas['msg']='没有获取到订单信息';
		   
            exit(json_encode($datas));
		}
		$map['order_id'] = $id;
        $map['user_id'] = $this->user_id;
        $order_info = M('order')->where($map)->find();
        $order_info = set_btn_order_status($order_info);  // 添加属性  包括按钮显示属性 和 订单状态显示属性
        if (!$order_info) {
           $datas['msg']='没有获取到订单信息';
		   
            exit(json_encode($datas));
        }
		else{
			$datas['flag']=1;
		}
        //获取订单商品
        $model = new UsersLogic();
        $data = $model->get_order_goods($order_info['order_id']);
		
		/* foreach($data['result'] as $k=>$v){
			$data['result'][$k]['add_time']=date('Y-m-d H:i:s',strtotime($data['result'][$k]['add_time']));
		} */
		
        $order_info['goods_list'] = $data['result'];
        //$order_info['total_fee'] = $order_info['goods_price'] + $order_info['shipping_price'] - $order_info['integral_money'] -$order_info['coupon_price'] - $order_info['discount'];

        $region_list = get_region_list();
        $invoice_no = M('DeliveryDoc')->where("order_id", $id)->getField('invoice_no', true);
        $order_info[invoice_no] = implode(' , ', $invoice_no);
        //获取订单操作记录
        $order_action = M('order_action')->where(array('order_id' => $id))->select();
       
		$datas['order_status']=C('ORDER_STATUS');
		$datas['shipping_status']=C('SHIPPING_STATUS');
		$datas['pay_status']=C('PAY_STATUS');
		
		$datas['orderInfo']=$order_info;
		$datas['orderAction']=$order_action;
		$datas['region_list']=$region_list;
		exit(json_encode($datas));
    }	
		
	 public function index()
		{
			$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:0;
			$user_id =$this->user_id;
			$logic = new UsersLogic();
			$user = $logic->get_info($user_id); //当前登录用户信息
			$comment_count = M('comment')->where("user_id", $user_id)->count();   // 我的评论数
			$level_name = M('user_level')->where("level_id", $this->user['level'])->getField('level_name'); // 等级名称
			//获取用户信息的数量
			$user_message_count = D('Message')->getUserMessageCount();
			
			$data['user_message_count']=$user_message_count;
			$data['level_name']=$level_name;
			$data['comment_count']=$comment_count;
			$data['user']=$user['result'];
			exit(json_encode($data));
			
		}	
		
	public function receive()
		{
			$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:2592;
			$where = ' user_id=' . $this->user_id;
			$datas=array('flag'=>0,'msg'=>'');
			//条件搜索
			$type=isset($_POST['type'])?$_POST['type']:'WAITRECEIVE';
			if ( strtoupper($type)== 'WAITRECEIVE') {
				$where .= C(strtoupper($type));
			}
			$count = M('order')->where($where)->count();
			$pagesize = C('PAGESIZE');
			$Page = new Page($count, $pagesize);
		   // $show = $Page->show();
			$order_str = "order_id DESC";
			$order_list = M('order')->order($order_str)->where($where)->limit($Page->firstRow . ',' . $Page->listRows)->select();
			if(empty($order_list)){
				$datas['msg']='没有搜索到数据';
				exit(json_encode($datas));
			}
			else{
				$datas['flag']=1;
			}
			//获取订单商品
			$model = new UsersLogic();
			foreach ($order_list as $k => $v) {
				$order_list[$k] = set_btn_order_status($v);  // 添加属性  包括按钮显示属性 和 订单状态显示属性
				//$order_list[$k]['total_fee'] = $v['goods_amount'] + $v['shipping_fee'] - $v['integral_money'] -$v['bonus'] - $v['discount']; //订单总额
				$data = $model->get_order_goods($v['order_id']);
				$order_list[$k]['goods_list'] = $data['result'];
			}

			//统计订单商品数量
			foreach ($order_list as $key => $value) {
				$count_goods_num = '';
				foreach ($value['goods_list'] as $kk => $vv) {
					$count_goods_num += $vv['goods_num'];
				}
				$order_list[$key]['count_goods_num'] = $count_goods_num;
				//订单物流单号
				$invoice_no = M('DeliveryDoc')->where("order_id", $value['order_id'])->getField('invoice_no', true);
				$order_list[$key][invoice_no] = implode(' , ', $invoice_no);
			}
			$datas['order_list']=$order_list;
			exit(json_encode($datas));
		}	
		
	 public function receiveConfirm()
		{
			$id = isset($_POST['order_id'])?$_POST['order_id']:0;//I('get.id/d', 0);
			$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:0;
			$data = confirm_order($id, $this->user_id);
			if ($data['status'] != 1) {
				$data['msg']='操作失败';
				exit(json_encode($data));
			} else {
				$model = new UsersLogic();
				$order_goods = $model->get_order_goods($id);
				$data['msg']='操作成功';
				$data['order_goods']=$order_goods;
				exit(json_encode($data));
			}
		}	
		
	 public function comment()
		{
			$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:2592;
			$user_id = $this->user_id;
			$status = isset($_POST['status'])?$_POST['status']:1;//I('get.status');
			$logic = new \app\common\logic\CommentLogic;
			$result = $logic->getComment($user_id, $status); //获取评论列表
			$data=array('flag'=>0,'msg'=>'');
			if(!empty($result['result'])){
				$data['flag']=1;
				
				foreach($result['result'] as $k => $v){
					$oimg=M('goods')->where('goods_id='.$result['result'][$k]['goods_id'])->field('original_img')->find();
					$result['result'][$k]['oimg']=$oimg['original_img'];
					$result['result'][$k]['add_time']=date('Y-m-d H:i:s',$result['result'][$k]['add_time']);
				}
				$data['comment']=$result['result'];
			}
			else{
				$data['msg']='没有搜索到数据';
			}
			exit(json_encode($data));
		}	
		
	 public function return_goods_list()
		{
			$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:1;
			//退换货商品信息
			$data=array('flag'=>0,'msg'=>'');
			$count = M('return_goods')->where("user_id", $this->user_id)->count();
			$pagesize = C('PAGESIZE');
			$page = new Page($count, $pagesize);
			$list = M('return_goods')->where("user_id", $this->user_id)->order("id desc")->limit("{$page->firstRow},{$page->listRows}")->select();
			$goods_id_arr = get_arr_column($list, 'goods_id');  //获取商品ID
			$goodsList=array();
			if (!empty($goods_id_arr)){
				$goodsList = M('goods')->where("goods_id", "in", implode(',', $goods_id_arr))->getField('goods_id,goods_name,original_img');
			}
			if(!empty($list)){
				$data['flag']=1;
				foreach($list as $k=>$v){
					$list[$k]['addtime']=date('Y-m-d H:i:s',$list[$k]['addtime']);
				}
			}
			else{
				$data['msg']='没有搜索到数据';
			}
			$data['status_desc']=array(
				'您的服务单已经取消',
				'很抱歉！您的服务单未通过审核',
				'您的服务单已申请成功，待售后审核中',
				'您的服务单已通过审核',
				'卖家已收到您寄回的物品,卖家已重新发货',
				'您的服务单完成'
			);
			$state = C('REFUND_STATUS');
			$data['goodsList']=$goodsList;
			$data['list']=$list;
			$data['state']=$state;
			exit(json_encode($data));
		}	
		
	/**
     * 取消售后服务
     * @author lxl
     * @time 2017-4-19
     */
    public function return_goods_cancel(){
        $id = isset($_POST['id'])?$_POST['id']:0;//I('id',0);
		$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:0;
		$data=array('flag'=>0,'msg'=>'');
        if(empty($id))$data['msg']='参数错误';
        $return_goods = M('return_goods')->where(array('id'=>$id,'user_id'=>$this->user_id))->find();
        if(empty($return_goods)) $data['msg']='参数错误';
        $s=M('return_goods')->where(array('id'=>$id))->save(array('status'=>-2,'canceltime'=>time()));
       if($s){
		    $data['flag']=1;
			$data['msg']='取消成功';
	   }
	   exit(json_encode($data));
    }


	 /**
     * 换货商品确认收货
     * @author lxl
     * @time  17-4-25
     * */
    public function returnReceiveConfirm(){
        $return_id=isset($_POST['id'])?$_POST['id']:0;//I('return_id/d');
        $return_info=M('return_goods')->field('order_id,order_sn,goods_id,spec_key')->where('id',$return_id)->find(); //查找退换货商品信息
        $update = M('return_goods')->where('id',$return_id)->save(['status'=>3]);  //要更新状态为已完成
        $data=array('flag'=>0,'msg'=>'');
		if($update) {
            $s=M('order_goods')->where(array(
                'order_id' => $return_info['order_id'],
                'goods_id' => $return_info['goods_id'],
                'spec_key' => $return_info['spec_key']))->save(['is_send' => 2]);  //订单商品改为已换货
           
			$data['flag']=1;
			$data['msg']='操作成功';
			
			//$this->success("操作成功", U("User/return_goods_info", array('id' => $return_id)));
        }
		else{
			$data['msg']='操作失败';
		}
       exit(json_encode($data));
    }

	/**
     *  退货详情
     */
    public function return_goods_info()
    {
        $id = isset($_POST['id'])?$_POST['id']:12;//I('id/d', 0);
		$data=array('flag'=>0,'msg'=>'');
        $return_goods = M('return_goods')->where("id = $id")->find();
        $return_goods['seller_delivery'] = unserialize($return_goods['seller_delivery']);  //订单的物流信息，服务类型为换货会显示
        if ($return_goods['imgs'])
            $return_goods['imgs'] = explode(',', $return_goods['imgs']);
        $goods = M('goods')->where("goods_id = {$return_goods['goods_id']} ")->find();
        $state = C('REFUND_STATUS');
		if(!empty($return_goods)){
			$data['flag']=1;
		}
		else{
			$data['msg']='没有搜索到数据';
		}
		foreach($return_goods as $v){
			$return_goods['addtime']=date('Y-m-d H:i:s',$return_goods['addtime']);
		}
		$data['state']=$state;
		$data['goods']=$goods;
		$data['return_goods']=$return_goods;
		exit(json_encode($data));
    }

	/*
     * 账户资金
     */
    public function account()
    {
        $user = session('user');
        //获取账户资金记录
		$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:0;
		$type=isset($_POST['type'])?$_POST['type']:0;
		
		$result=M('users')->where('user_id='.$this->user_id)->field('user_id,user_money')->find();
		if(empty($result)){
			$result['user_money']=0;
		}
		exit(json_encode($result));
		
		/* 
        $logic = new UsersLogic();
        $data = $logic->get_account_log($this->user_id, I('get.type'));
        $account_log = $data['result'];

        $this->assign('user', $user);
        $this->assign('account_log', $account_log);
        $this->assign('page', $data['show']);

        if ($_GET['is_ajax']) {
            return $this->fetch('ajax_account_list');
            exit;
        }
        return $this->fetch(); */
    }

	 public function recharge()
		{
			$order_id = 0;//I('order_id/d');
			$paymentList = M('Plugin')->where("`type`='payment' and code!='cod' and status = 1 and  scene in(0,1)")->select();
			//微信浏览器
			if (strstr($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger')) {
				$paymentList = M('Plugin')->where("`type`='payment' and status = 1 and code='weixin'")->select();
			}
			$paymentList = convert_arr_key($paymentList, 'code');

			foreach ($paymentList as $key => $val) {
				$val['config_value'] = unserialize($val['config_value']);
				if ($val['config_value']['is_bank'] == 2) {
					$bankCodeList[$val['code']] = unserialize($val['bank_code']);
				}
			}
			$bank_img = include APP_PATH . 'home/bank.php'; // 银行对应图片
			$payment = M('Plugin')->where("`type`='payment' and status = 1")->select();
			$data['paymentList']=$paymentList;
			$data['bank_img']=$bank_img;
			$data['bankCodeList']=$bankCodeList;
			exit(json_encode($data));
			/* $this->assign('paymentList', $paymentList);
			$this->assign('bank_img', $bank_img);
			$this->assign('bankCodeList', $bankCodeList) */;

			/* if ($order_id > 0) {
				$order = M('recharge')->where("order_id", $order_id)->find();
				$this->assign('order', $order);
			}
			return $this->fetch(); */
		}

	/**账户明细*/
    public function points()
		{
			$type = isset($_POST['type'])?$_POST['type']:0;//I('type', 'all');    //获取类型
			$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:1;
			$typeField=array('all','recharge','points');
			//$this->assign('type', $type);
			$data=array('flag'=>0,'msg'=>'');
			$data['type']=$type;
			$type=$typeField[$type];
			if ($type == 'recharge') {
				//充值明细
				$count = M('recharge')->where("user_id", $this->user_id)->count();
				$Page = new Page($count, 16);
				$account_log = M('recharge')->where("user_id", $this->user_id)->order('order_id desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();
			} else if ($type == 'points') {
				//积分记录明细
				$count = M('account_log')->where(['user_id' => $this->user_id, 'pay_points' => ['<>', 0]])->count();
				$Page = new Page($count, 16);
				$account_log = M('account_log')->where(['user_id' => $this->user_id, 'pay_points' => ['<>', 0]])->order('log_id desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();
			} else {
				//全部
				$count = M('account_log')->where(['user_id' => $this->user_id])->count();
				$Page = new Page($count, 16);
				$account_log = M('account_log')->where(['user_id' => $this->user_id])->order('log_id desc')->limit($Page->firstRow . ',' . $Page->listRows)->select();
			}
			
			foreach($account_log as $k =>$v){
				if(isset($account_log[$k]['ctime'])){
					$account_log[$k]['ctime']=date('Y-m-d H:i:s',$account_log[$k]['ctime']);
				}
				
				if(isset($account_log[$k]['change_time'])){
					$account_log[$k]['change_time']=date('Y-m-d H:i:s',$account_log[$k]['change_time']);
				}
			}
			
			if(!empty($account_log)){
				$data['flag']=1;
				$data['account_log']=$account_log;
			}
			else{
				$data['msg']='没有搜索到数据';
			}
			
			exit(json_encode($data));
			/* 
			$showpage = $Page->show();
			$this->assign('account_log', $account_log);
			$this->assign('page', $showpage);
			$this->assign('listRows', $Page->listRows);
			if ($_GET['is_ajax']) {
				return $this->fetch('ajax_points');
				exit;
			}
			return $this->fetch(); */
		}

	 /**
     * 申请记录列表
     */
    public function withdrawals_list()
    {
		$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:0;
        $withdrawals_where['user_id'] = $this->user_id;
        $count = M('withdrawals')->where($withdrawals_where)->count();
        $pagesize = C('PAGESIZE');
        $page = new Page($count, $pagesize);
        $list = M('withdrawals')->where($withdrawals_where)->order("id desc")->limit("{$page->firstRow},{$page->listRows}")->select();
		$data=array('flag'=>0,'msg'=>'');
		if(!empty($list)){
			$data['flag']=1;
			$data['list']=$list;
		}
		else{
			$data['msg']='没有搜索到数据';
		}
		exit(json_encode($data));
    }

	/**
     * 优惠券
     */
    public function coupon()
    {
		$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:1;
		$type=isset($_POST['type'])?$_POST['type']:2;
        $logic = new UsersLogic();
        $data = $logic->get_coupon($this->user_id, $type);
        $coupon_list = $data['result'];
		$datas=array('flag'=>0,'msg'=>'');
		foreach($coupon_list as $k=>$v){
			//$st=M('coupon_list')->where('cid='.$v['cid'].' and uid='.$this->user_id)->field('id,status')->find();
			
			//elseif($st['status']){}
			$coupon_list[$k]['use_end_time']=date('Y-m-d H:i:s',$coupon_list[$k]['use_end_time']);
			
			$coupon_list[$k]['ustatus']=$type;
		}
		if(!empty($coupon_list)){
			$datas['flag']=1;
			$datas['coupon_list']=$coupon_list;
		}
		else{
			$datas['msg']='没有搜索到数据';
		}
		exit(json_encode($datas));
		
        /* $this->assign('coupon_list', $coupon_list);
        $this->assign('page', $data['show']);
        if (input('is_ajax')) {
            return $this->fetch('ajax_coupon_list');
            exit;
        }
        return $this->fetch(); */
    }

	/*
     * 个人信息
     */
    public function userinfo()
    {
		$this->user_id=isset($_POST['user_id'])?$_POST['user_id']:1;
        $userLogic = new UsersLogic();
        $user_info = $userLogic->get_info($this->user_id); // 获取用户信息
        $user_info = $user_info['result'];
		
		$data=array('flag'=>0,'msg'=>'');
        /* if (IS_POST) {
			
        	if ($_FILES['head_pic']['tmp_name']) {
        		$file = $this->request->file('head_pic');
        		$validate = ['size'=>1024 * 1024 * 3,'ext'=>'jpg,png,gif,jpeg'];
        		$dir = 'public/upload/head_pic/';
        		if (!($_exists = file_exists($dir))){
        			$isMk = mkdir($dir);
        		}
        		$parentDir = date('Ymd');
        		$info = $file->validate($validate)->move($dir, true);
        		if($info){
        			$post['head_pic'] = '/'.$dir.$parentDir.'/'.$info->getFilename();
        		}else{
        			$this->error($info->getError());//上传错误提示错误信息
        		}
        	}
			
			
            I('post.nickname') ? $post['nickname'] = I('post.nickname') : false; //昵称
            I('post.qq') ? $post['qq'] = I('post.qq') : false;  //QQ号码
            I('post.head_pic') ? $post['head_pic'] = I('post.head_pic') : false; //头像地址
            I('post.sex') ? $post['sex'] = I('post.sex') : $post['sex'] = 0;  // 性别
            I('post.birthday') ? $post['birthday'] = strtotime(I('post.birthday')) : false;  // 生日
            I('post.province') ? $post['province'] = I('post.province') : false;  //省份
            I('post.city') ? $post['city'] = I('post.city') : false;  // 城市
            I('post.district') ? $post['district'] = I('post.district') : false;  //地区
            I('post.email') ? $post['email'] = I('post.email') : false; //邮箱
            I('post.mobile') ? $post['mobile'] = I('post.mobile') : false; //手机

            $email = I('post.email');
            $mobile = I('post.mobile');
            $code = I('post.mobile_code', '');
            $scene = I('post.scene', 6);

            if (!empty($email)) {
                $c = M('users')->where(['email' => input('post.email'), 'user_id' => ['<>', $this->user_id]])->count();
                $c && $this->error("邮箱已被使用");
            }
            if (!empty($mobile)) {
                $c = M('users')->where(['mobile' => input('post.mobile'), 'user_id' => ['<>', $this->user_id]])->count();
                $c && $this->error("手机已被使用");
                if (!$code)
                    $this->error('请输入验证码');
                $check_code = $userLogic->check_validate_code($code, $mobile, 'phone', $this->session_id, $scene);
                if ($check_code['status'] != 1)
                    $this->error($check_code['msg']);
            }

            if (!$userLogic->update_info($this->user_id, $post))
                $this->error("保存失败");
            setcookie('uname',urlencode($post['nickname']),null,'/');
            $this->success("操作成功");
            exit;
        } */
		if(!empty($user_info)){
			$data['flag']=1;
			$data['user_info']=$user_info;
		}
		else{
			$data['msg']='没有搜索到数据';
		}
        //  获取省份
        $province = M('region')->where(array('parent_id' => 0, 'level' => 1))->select();
        //  获取订单城市
        $city = M('region')->where(array('parent_id' => $user_info['province'], 'level' => 2))->select();
        //  获取订单地区
        $area = M('region')->where(array('parent_id' => $user_info['city'], 'level' => 3))->select();
        $data['user_info']=$user_info;
		$data['province']=$province;
		$data['city']=$city;
		$data['area']=$area;
		$data['sex']=C('SEX');
		exit(json_encode($data));
		
    }
	
	 public function logout()
		{
			session_unset();
			@session_destroy();
			setcookie('uname','',time()-3600,'/');
			setcookie('cn','',time()-3600,'/');
			setcookie('user_id','',time()-3600,'/');
			setcookie('PHPSESSID','',time()-3600,'/');
			$data['flag']=1;
			exit(json_encode($data));
		}
	
	protected function isEmail($str){
		$preg='/^([0-9A-Za-z\-_\.]+)@([0-9a-z]+\.[a-z]{2,3}(\.[a-z]{2})?)$/';
		return preg_match($preg,$str);
	}
	
	protected function isMobile($str){
		$preg='/1[0-9]{10}/';
		return preg_match($preg,$str);
	}
	
	protected function isPassword($str){
		$preg='/^(?=.*[a-zA-Z])(?=.*\d)(?=.*[~!@#$%^&*()_+`\-={}:";\'<>?,.\/]).{6,18}$/';
		return preg_match($preg,$str);
	}
	
	public function updateUserinfo(){
		$type=isset($_POST['type'])?intval($_POST['type']):5;
		$user_id=isset($_POST['user_id'])?intval($_POST['user_id']):1;
		$userinfo=isset($_POST['userinfo'])?$_POST['userinfo']:array();
		$data=array('flag'=>0,'msg'=>'参数错误');
		$userLogic = new UsersLogic();
		$preg='';
		
		if($type==2){
			$preg='/^[\x80-\xff_a-zA-Z]{3,16}$/';
			if(!preg_match($preg,$userinfo['nickname'])){
				$data['msg']='用户名格式错误';
				exit(json_encode($data));
			}
			
		}
		
		if($type==3){
			if($userinfo['sex']!=1 && $userinfo['sex']!=2){
				$data['msg']='性别格式错误'.$userinfo['sex'];
				exit(json_encode($data));
			}
			
		}
		
		if($type==4){
			exit(json_encode($data));
		}
		
		if($type==5){
			$userinfo['email']='1763049679@qq.com';
			if(!$this->isEmail($userinfo['email'])){
				$data['msg']='邮件格式错误';
				exit(json_encode($data));
			}
			
			$c = M('users')->where(['email' => $userinfo['email'], 'user_id' => ['<>', $user_id]])->count();
			if($c){
				$data['msg']="邮箱已被使用";
				exit(json_encode($data));
			}
                
		}
		
		if($type==6){
			if(!$this->isPassword($userinfo['password1'])){
				$data['msg']='密码格式错误';
				exit(json_encode($data));
			}
			
			if(!$this->isPassword($userinfo['password2'])){
				$data['msg']='新密码格式错误';
				exit(json_encode($data));
			}
			
			if(!$this->isPassword($userinfo['password3'])){
				$data['msg']='确认密码格式错误';
				exit(json_encode($data));
			}
			
			if($userinfo['password2']!=$userinfo['password3']){
				$data['msg']='两次输入的密码不一致';
				exit(json_encode($data));
			}
			$userinfo['password']=sha1(md5($userinfo['password3']));
			unset($userinfo['password1']);
			unset($userinfo['password2']);
			unset($userinfo['password3']);
			
			//
		}
		if(!empty($user_id) && !empty($type) && !empty($userinfo)){
			if(!$userLogic->update_info($user_id,$userinfo)){
				$data['msg']='操作失败';
				exit(json_encode($data));
			}
			else{
				$data['flag']=1;
				$data['msg']='操作成功';
				exit(json_encode($data));
			}
		}
		exit(json_encode($data));
	}
	
	function uheader(){
		$data['status']=1;
		exit(json_encode($data));
	}
	
	
	
	
	
	
		
		
}









?>