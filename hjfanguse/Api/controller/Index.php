<?php
namespace app\Api\controller;
use app\home\logic\UsersLogic;
use Think\Db;
use think\Page;

header('content-type:application:json;charset=utf8');  
header('Access-Control-Allow-Origin:*');  
header('Access-Control-Allow-Methods:POST');  
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT,DELETE');

class Index{

	public function index(){
		$p=isset($_POST['page'])?intval($_POST['page']):1;
		$hot_goods = M('goods')->where("is_hot=1 and is_on_sale=1")->order('goods_id ASC')->limit(20)->select();//->cache(true,TPSHOP_CACHE_TIME)首页热卖商品
		$new_goods = M('goods')->where("is_on_sale=1")->order('goods_id DESC')->limit(20)->select();//->cache(true,TPSHOP_CACHE_TIME)首页热卖商品
		$favourite_goods = M('goods')->where("is_recommend=1 and is_on_sale=1")->order('goods_id ASC')->page($p,C('PAGESIZE'))->select();//->cache(true,TPSHOP_CACHE_TIME)首页推荐商品
    	$data['hot_goods']=$hot_goods;
		$data['new_goods']=$new_goods;
		$data['favourite_goods']=$favourite_goods;
		exit(json_encode($data));
	}
	
	public function getCategory(){
		
		$cate=M('goods_category')->field('id,parent_id,parent_id_path,`name`,is_show,`level`')->order('id asc')->select();
		$cates=array();
		$i=0;
		foreach($cate as $k => $v){
			if($v['level']==2){
				$cates['parent'][$i++]=$v;
				unset($cate[$k]);
			}
		}
		$j=0;
		foreach($cates['parent'] as $ks=>$vs){
			foreach($cate as $key=>$val){
				$val['name']=mb_substr($val['name'],0,4);
				if($val['parent_id']==$vs['id']){
					$cates['parent'][$ks]['child'][$j++]=$val;
					unset($cate[$ks]);
				}
			}
		}
		unset($cate);
		exit(json_encode($cates));
	}
	
	public function search(){
		$val=isset($_POST['search_val'])?trim($_POST['search_val']):0;
		$page=isset($_POST['page'])?intval($_POST['page']):1;
		$val=strip_tags($val);
		/* if(!get_magic_quotes_gpc()){
			$val=addslashes($val);
		} */
		$data=array('flag'=>0,'msg'=>'没有数据');
		$where  = array('is_on_sale' => 1);
		if(!empty($val)){
			$data['flag']=1;
			$where['goods_name'] = array('like','%'.$q.'%');
			//$count=M('goods')->where('is_real=1 and is_on_sale=1'.$cat_id_str.' and store_count > 0')->count();
			
			
			$goodsLogic = new \app\home\logic\GoodsLogic(); // 前台商品操作逻辑类
			$filter_goods_id = M('goods')->where($where)->cache(true)->getField("goods_id",true);
			$count = count($filter_goods_id);
			
			$page=$page > 0 ?$page : 1;
			$number=20;
			$start=($page-1)*$number;
			$end=$start+$number;
			
				if($start < $count)
					{
						if($end > $count)
							{
								$number=$number-($end-$count);
							}
					}
			$page = new Page($count,$number);
			$page->firstRow=$start;
			$page->listRows=$number;
			
			$goods_list = M('goods')->where("goods_id", "in", implode(',', $filter_goods_id))->order("goods_id desc")->limit($page->firstRow.','.$page->listRows)->select();
    		$data['goods_list']=$goods_list;
			$data['total']=$count;
		}
		exit(json_encode($data));
	}
	
	/**
     * 商品活动页面
	 *
	 **/
	 public function promote_goods(){
		$page=isset($_POST['page'])?intval($_POST['page']):1;
		$data=array('flag'=>0,'msg'=>'没有数据');
        $now_time = time();
        $where = " start_time <= $now_time and end_time >= $now_time ";
        $count = M('prom_goods')->where($where)->count();  // 查询满足要求的总记录数
        $page=$page > 0 ?$page : 1;
			$number=20;
			$start=($page-1)*$number;
			$end=$start+$number;
			
				if($start < $count)
					{
						if($end > $count)
							{
								$number=$number-($end-$count);
							}
					}
			$page = new Page($count,$number);
			$page->firstRow=$start;
			$page->listRows=$number;
       $promote = M('prom_goods')->field('id,name,start_time,end_time,prom_img')->where($where)->limit($Page->firstRow.','.$Page->listRows)->select();    //查询活动列表
       if(!empty($promote)){
		   foreach($promote as $k => $v){
			   $promote[$k]['start_time']=date('Y-m-d H:i:s',$promote[$k]['start_time']);
			   $promote[$k]['end_time']=date('Y-m-d H:i:s',$promote[$k]['end_time']);
		   }
		    $data['total']=$count;
			$data['promote']=$promote;
			$data['flag']=1;
	   }
	  
	  exit(json_encode($data));
    }
	
	/**
     * 活动商品列表
     */
    public function promote_list(){
        $prom_id =isset($_POST['id'])?intval($_POST['id']):0;//活动ID
		$page =isset($_POST['page'])?intval($_POST['page']):1;
		$data=array('flag'=>0,'msg'=>'没有数据');
        $where = array(     //条件
            'prom_type'=>3,
            'prom_id'=>$prom_id,
        );
        $pagesize = C('PAGESIZE');  //每页显示数
    	$count =  M('goods')->where($where)->count(); // 查询满足要求的总记录数
    	$page=$page > 0 ?$page : 1;
			$number=20;
			$start=($page-1)*$number;
			$end=$start+$number;
			
				if($start < $count)
					{
						if($end > $count)
							{
								$number=$number-($end-$count);
							}
					}
			$page = new Page($count,$number);//分页类
			$page->firstRow=$start;
			$page->listRows=$number; 
        $prom_list = M('goods')->where($where)->limit($Page->firstRow.','.$Page->listRows)->select(); //活动对应的商品
    	if(!empty($prom_list)){
			$data['total']=$count;
			$data['prom_list']=$prom_list;
			$data['flag']=1;
		}
		exit(json_encode($data));
    }
	
	
	
	


}


?>